# Modelling
This is a repository for library code dedicated to simulating (ideally with some form of rendering) for the `computing.hardware.autonomy` project as a path towards decentralised and self-replicable semiconductor manufacture. This will be worked on over time - feel free to contribute.

For more details on why this repository is necessary, see [the fabrication repository][repository-fabrication].

## License
This code is all licensed - unless otherwise specified - under `AGPL-3.0-or-later OR CERN-OHL-S-2.0+`. The documentation is licensed under these licenses as well as `CC-BY-SA-4.0 OR GFDL-1.2-or-later`, for RepRap wiki compatibility - see [licensing] for information on why it's so complex.

By contributing to this repository, you agree that you're contributing code/documentation/etc. to this repository under a licensing scheme compatible with the respective licenses.

[repository-fabrication]: https://gitlab.com/computing.hardware.autonomy/fabrication
[licensing]: https://gitlab.com/computing.hardware.autonomy/gitlab-profile#licensing
