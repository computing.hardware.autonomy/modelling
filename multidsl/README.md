# multidsl
This crate is an attempt to figure out modelling for physical simulations using a mix of "code structure DSLs" (where GPU compute is controlled by language constructions that can then be used to deduce stuff like types, code, etc. - genericity may enable CPU compute too via WASM and JIT, but we won't try and do compile-time CPU things because it makes complexity obscene) for stuff like grid operations, tensor stuff, etc. It's quite experimental and changing all the time right now, in very early stages.

For example, one part of a program may be a sophisticated lattice-boltzmann model, while another part may enable complex user-controlled boundary conditions. These different components may require different optimisation strategies.

Taking influence from [the paper using multi-level intermediate-representation to optimise stencil operations][mlir-stencils] to boost performance (important given that the author is running on very limited hardware) for specific problem classes, this crate attempts to do something similar except by using different optimisation strategies for different subcomponents of a program while allowing those subcomponents to link together. 

This involves the use of Rust code to directly define various structures in a program-to-be-run, rather than using a "normal" shader language. This avoids needing to figure out parsing and all that stuff. 

This crate will likely also be split and renamed in future - it's (again) highly experimental early stages and I'm still figuring out the broad design structure and getting familiar with Tagless Final Style as well.

([MLIR][mlir]). Also, consider [full space and time (for cache) loop tiling](https://www.researchgate.net/publication/354547704_Space-Time_Loop_Tiling_for_Dynamic_Programming_Codes)

## Things we will need
* We need a CPU/GPU generic ABI abstraction layer for low level types
* We need abstractions for functions and entrypoints built with the ABI - consider using closures for block constructions. 
* We need "higher level" ABI structures specific to arrays, including automatic sharding for compute workgroups and such (both within a single invokation for parallelization, and across multiple invokations for larger datasets). Basing things on data proximity and using this to calculate arrangements is important. 
* This needs to be semi-dynamic to enable calculating higher-level bulk ABI structures based on cache sizes, etc. for optimisation. For instance, semi-dynamic conversions from standard array order (a-la row- or column- major for 2D), to notions of n-dimensional blocks in other orders like hilbert order or similar.
* Dynamism is acceptable in translating from CPU -> GPU representation as the idea of this crate is that all the heavy computation is primarily on-gpu. Some sharding may occur on CPU (e.g. invoking on subsets of a database), but things should be stored in a format friendly for the on-GPU computations other than right at the end or start. 
* Another target is also figuring out modification of physical computations such that floating point values don't hit extreme largeness or get too close to zero - that is, "unit optimisation". Hence we need to truly encode the concept of units into the higher level abstraction and the notion of scaling into the calculated ABI, with the calculation system generic enough to enable this sort of thing. 

## Tagless Final Style
We will most likely use significant techniques from [tagless final style][tagless-final-style]. This project is still very experimental, so things may change a lot. The actual design techniques are likely to change or be modified. 


[mlir-stencils]: https://dl.acm.org/doi/fullHtml/10.1145/3469030
[mlir]: https://mlir.llvm.org/users/
[tagless-final-style]: https://dev.to/kurt2001/efficient-extensible-expressive-typed-tagless-final-interpreters-in-rust-l71
