//! This is data structures used to construct expressions and equations in abstract form,
//! suitable for abstract manipulation into other forms.
//!
//! # Variables
//! Variables in equations are represented by unique types that act as names, which are then
//! proxied to "equivalent" types via implementing

pub mod modes;
pub mod operations;
pub mod var;

/// Identifier of variables - variables use TypeID as their unique identifiers.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
pub struct VarId(core::any::TypeId);

impl VarId {
    #[inline(always)]
    pub fn of<T: var::Variable>() -> Self {
        Self(core::any::TypeId::of::<T>())
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn basic_test() {}
}

// SPDX-License-Identifier: AGPL-3.0-or-later OR CERN-OHL-S-2.0+
