//! Modes represent ways of actually turning an abstract sequence of operations into a
//! concrete behaviour (whether that's by producing a value, creating a script, etc.)
//!
//! Implementing traits on modes - corresponding to various operations - is how an abstract
//! algebraic implementation is turned into concrete behaviour (or is turned into another,
//! distinct, abstract implementation).

pub mod concrete;
pub mod modifiers;

/// Actual evaluation mode.
///
/// May in future contain more inner associated types, which may want to use information from
/// [`DataRepresentable::ReprInfo`].
pub trait Mode {
    /// Wrapper for [`Resolvable::ResolvedData`] when being outputted. This is likely what you want
    /// to wrap anything in when writing implementations on operations for a mode.
    type WrapResolved<Data>
    where
        Self: DataRepresentable<Data>;
}

/// Implement for data that can be represented in the given mode.
///
/// Some modes may want to not constrain their output representations at all, in which case a
/// blanket impl is useful.
///
/// This can be used e.g. to create modes that only allow outputting data types with certain
/// constraints (like being GPU friendly or something).  
pub trait DataRepresentable<Data>: Mode {
    type ReprInfo;
}

/// This is the trait by which your mode should resolve different expressions e.g. resolve
/// various of the [`crate::algebra::operations`] with parameters that may themselves have
/// resolve implemented with them as expressions (should the mode allow it).
pub trait Resolvable<Expression: ?Sized>: DataRepresentable<Self::ResolvedData> + Mode {
    type ResolvedData;

    fn resolve_expression(&mut self, expression: &Expression) -> Self::WrapResolved<Self::ResolvedData>;
}

// SPDX-License-Identifier: AGPL-3.0-or-later OR CERN-OHL-S-2.0+
