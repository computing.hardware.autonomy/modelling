//! Concrete modes of behaviour.

use core::ops::{Add, Neg, Sub};

use super::{DataRepresentable, Mode, Resolvable};
use crate::algebra::operations::{self, basic::additive};

// TODO: Implement convenience traits to enable automatic per-operation impls for common
// co-habiting operations and data-types (rather than indirect via variable access).

/// Most basic concrete [`super::Mode`]. It plain ol' evaluates expressions as constructed. All
/// data is representable.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct Eval;

impl Mode for Eval {
    type WrapResolved<Data>
        = Data
    where
        Self: super::DataRepresentable<Data>;
}

impl<T> DataRepresentable<T> for Eval {
    type ReprInfo = ();
}

impl<A> Resolvable<additive::Inverse<A>> for Eval
where
    Self: Resolvable<A, ResolvedData: Neg>,
{
    type ResolvedData = <<Self as Resolvable<A>>::ResolvedData as Neg>::Output;

    #[inline]
    fn resolve_expression(
        &mut self,
        expression: &additive::Inverse<A>,
    ) -> Self::WrapResolved<Self::ResolvedData> {
        -self.resolve_expression(&expression.0)
    }
}

impl<A, B> Resolvable<additive::Add<A, B>> for Eval
where
    Self: Resolvable<A, ResolvedData: Add<<Self as Resolvable<B>>::ResolvedData>>,
    Self: Resolvable<B>,
{
    type ResolvedData = <<Self as Resolvable<A>>::ResolvedData as Add<
        <Self as Resolvable<B>>::ResolvedData,
    >>::Output;

    #[inline]
    fn resolve_expression(
        &mut self,
        expression: &additive::Add<A, B>,
    ) -> Self::WrapResolved<Self::ResolvedData> {
        self.resolve_expression(&expression.0) + self.resolve_expression(&expression.1)
    }
}

impl<A, B> Resolvable<additive::Subtract<A, B>> for Eval
where
    Self: Resolvable<A>,
    Self: Resolvable<B>,
    <Self as Resolvable<A>>::ResolvedData: Sub<<Self as Resolvable<B>>::ResolvedData>,
{
    type ResolvedData = <<Self as Resolvable<A>>::ResolvedData as Sub<
        <Self as Resolvable<B>>::ResolvedData,
    >>::Output;

    fn resolve_expression(
        &mut self,
        expression: &additive::Subtract<A, B>,
    ) -> Self::WrapResolved<Self::ResolvedData> {
        self.resolve_expression(&expression.0) - self.resolve_expression(&expression.1)
    }
}

// SPDX-License-Identifier: AGPL-3.0-or-later OR CERN-OHL-S-2.0+
