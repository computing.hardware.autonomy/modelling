//! Ways of working with variables/constants/etc.

pub mod basic {
    pub mod additive {
        /// Additive inverse (negation).
        #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
        pub struct Inverse<A>(pub A);

        /// Addition
        #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
        pub struct Add<A, B>(pub A, pub B);

        /// Subtraction
        #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
        pub struct Subtract<A, B>(pub A, pub B);
    }

    pub mod multiplicative {
        /// Multiplying of A and B
        #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
        pub struct Multiply<A, B>(pub A, pub B);

        /// Take something to a "compile time" integer exponent (positive only)
        #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
        pub struct UIntExponent<A, const N: u32>(pub A);
    }

    pub mod division {
        /// Multiplicative inverse (right and left).
        #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
        pub struct Inverse<A>(pub A);

        /// Dividing A by B
        #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
        pub struct Divide<A, B>(pub A, pub B);

        /// Take something to a "compile time" integer exponent
        /// power (positive or negative)
        #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
        pub struct IntExponent<A, const N: i32>(pub A);
    }
}
