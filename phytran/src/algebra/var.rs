//! Represents specifically variables/constants/etc. and their access. 

use core::any;

/// Type acting as variable name.
pub trait Variable: any::Any {}

// SPDX-License-Identifier: AGPL-3.0-or-later OR CERN-OHL-S-2.0+ 
