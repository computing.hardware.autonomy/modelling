#![cfg_attr(any(feature="std", test), no_std)]
pub mod algebra;

// Some credits: 
// * `https://crates.io/crates/gad` - the way algebras are handled in this is likely to be
//   an inspiration for the structure of some parts of this crate.

// Things we need:
// * some system to do and register *stencil operations* on grids, from arbitrary cell 
//   sub-variables - including as vectors. This should tie into the dimensions of each grid
//   and allow for some checking of operations, multi-device/gpu/workgroup sharding, 
//   and potentially automated transfer of "edge boundaries" where possible.
// * some system to deduce approximate *scale ranges* and adjust storage modes appropriately.
// * the ability to do symbolic or "pseudo symbolic" forward-automatic differentiation for various
//   cases. 
// * The ability to use stencils as well as device layout/cache/etc. information to modify the 
//   layout of grids in memory e.g. for lattice boltzmann allowing deferred steps to update grid
//   shard boundaries between workgroups/devices (on machine or cross network) nya.
// * definition of boundaries
// * Non-grid algebraic operations that use grid dimensions e.g. some Signed Distance Field
//   boundary, optionally with some kind of caching or sub-cell accesses in a coherent way. 
// * Encode generalisable vectorisation structure. 
// 
//
//
// Idea 1:
// * Separate out accesses (reads and writes) of each grid cell as well as any stencil
//   applications. This can be quite granular, and must allow us to do things like make 
//   functioning GPU synchronisation behaviour (or e.g. set up a subset of the executing 
//   workgroup invokations to write to some secondary/tertiary storage for the boundaries - 
//   this is where customising layouts comes into play) - there's also an easy solution of
//   having duplicated memory and swapping which avoids all synchronisation other than 
//   for broad computation. 
// * Use bevy-style types-as-names - this gets more complex as we want to easily have 
//   "identical" properties for multiple parts of a grid or grids (e.g. we may want to have 
//   grid parts for electrons, +ive ions, neutral, etc.). Potential solution is to allow types
//   tagged with context or something, - could make Newtypes easy to make but that would be 
//   more complex and require a lot of macros if we're going full abstraction. 
//   Such a context construction requires genericity - we want to allow making functionality 
//   that is generic (e.g. thermal relaxation with minor constant changes for some different 
//   lattice-boltzmann components, but that use the same underlying variables).

// SPDX-License-Identifier: AGPL-3.0-or-later OR CERN-OHL-S-2.0+ 
